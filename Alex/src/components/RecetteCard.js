export class RecetteCard {
  _photo;
  _titre;
  constructor(photo, titre) {
    this._photo = photo;
    this._titre = titre;

    console.log("New recettecarte", this._photo, this._titre);
  }

  get content() {
    const recette = document.createElement("recette");
    recette.classList.add("recette");

    const markup = `
    <div class="container text-center">
    <div class="row align-items-start"> 
      <li class="recette" id="recette">
        <div class="row justify-content-around col-2">
  
              <div class="detail"> 
              
               <img class= "recetteImage" src="${this._photo}">
                   
                
                        <h5 class="recetteTitre">${this._titre}</h5>
                        <p class="recetteInstruction">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" id="card1" class="btn btn-primary">Go somewhere</a>
                    </div>
            </div>
          </li>
  </div>


                    `;

    recette.innerHTML = markup;
    return recette;
  }
}
