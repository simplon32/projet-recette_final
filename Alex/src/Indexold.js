import { RecetteCard } from "./components/RecetteCard.js";

import { Recette } from "./classes/Recette";
import { Ingredient } from "./classes/Ingredient";
let urlStart = "https://www.themealdb.com/api/json/v1/1/search.php?s=";
let valider = document.getElementById("search-addon");
let recettes = document.getElementById("recettes");

const apiKey = "1";

const form = document.querySelector(".rechercher form");
const input = document.querySelector(".rechercher input");
const list = document.querySelector(".recette");

const urlStar = "https://www.themealdb.com/api/json/v1/1/search.php?s=";
//const rechercheCategorie = document.querySelector(".recetteCategorie");
form.addEventListener("submit", onFormSubmitted);

function onFormSubmitted(event) {
  // ne recharge pas la page (qui est le comportement par défaut)
  event.preventDefault(apiKey);

  let inputValue = input.value;
  let resultatrecherche;

  const url = `${urlStar}${inputValue}`;
  console.log("URL", url);

  fetch(url)
    .then((response) => response.json())
    .then(function (data) {
      console.log("hello");
      console.log(data);
      let strMealThumb = data.meals[0].strMealThumb;
      let strMeal = data.meals[0].strMeal;
      console.log(strMealThumb, data.meals[0].strMeal);
      console.log("salut");
      let ficherecette = new RecetteCard(strMealThumb, strMeal);

      console.log(ficherecette.content);
      list.appendChild(ficherecette.content);
    });
}

console.log("recettes aléatoires");

const random = `https://www.themealdb.com/api/json/v1/1/random.php`;

for (let compteur = 0; compteur <= 5; compteur++) {
  fetch(random)
    .then((response) => response.json())
    .then(function (data) {
      //  let Recette = new RecetteCard();
      console.log(data);

      const { strMealThumb, strMeal } = data.meals[0];

      let ficherecette = new RecetteCard(strMealThumb, strMeal);
      console.log(ficherecette.content);
      list.appendChild(ficherecette.content);
    });
}

valider.addEventListener("click", rechercheRecette);

function rechercheRecette(event) {
  event.preventDefault();
  let inputValue = document.getElementById("search").value;
  console.log(inputValue);
  let url = `${urlStart}${inputValue}`;
  fetch(url)
    .then((response) => response.json())
    .then(function (data) {
      // console.log(data)
      let i = 1;
      let ingredient = "";
      for (let meal of data.meals) {
        const {
          idMeal,
          strMeal,
          strCategory,
          strArea,
          strInstructions,
          strMealThumb,
          strIngredient1,
        } = meal;
        console.log(
          idMeal,
          strMeal,
          strCategory,
          strArea,
          strInstructions,
          strMealThumb,
          strIngredient1
        );
        let ingredients = [];
        let mesures = [];
        ingredient = "strIngredient" + i;
        const { ingredient } = meal;
        // console.log('strIngredient' + i );
        // console.log(ingredient );

        // if(ingredient != "" ){

        //     ingredients.push (ingredient);
        //     // mesures.push (mesure);
        //     i++
        //     console.log(ingredients);
        // }
        // else{console.log("pas d'ingredients");}
        // console.log("les ingredients:")
        // console.log(ingredients);
        // console.log(mesures);
        // let recette = new Recette(idMeal, strMeal, strCategory, strArea, strInstructions,strMealThumb, ingredients, mesures );
        // console.log (recette)
        // List.appendChild(recette.content);
      }
    })
    .catch(function (error) {
      console.log(error);
    });
}

for (let i = 0; i < 6; i++) {
  fetch("https://www.themealdb.com/api/json/v1/1/random.php")
    .then((response) => response.json())
    .then(function (data) {
      const {
        idMeal,
        strMeal,
        strCategory,
        strArea,
        strInstructions,
        strMealThumb,
      } = data.meals[0];
      console.log(strMeal);
      // console.log(data)
      // let recette = new Recette(idMeal, strMeal, strCategory, strArea, strInstructions, strMealThumb)
      // List.appendChild( recette.card)
    });
}
