import { RecetteCard } from "./components/RecetteCard.js";

const apiKey = "1";

const form = document.querySelector(".rechercher form");
const input = document.querySelector(".rechercher input");
const list = document.querySelector(".recette");

const urlStart = "https://www.themealdb.com/api/json/v1/1/search.php?s=";
//const rechercheCategorie = document.querySelector(".recetteCategorie");
form.addEventListener("submit", onFormSubmitted);

function onFormSubmitted(event) {
  // ne recharge pas la page (qui est le comportement par défaut)
  event.preventDefault(apiKey);

  let inputValue = input.value;
  let resultatrecherche;

  const url = `${urlStart}${inputValue}`;
  console.log("URL", url);

  fetch(url)
    .then((response) => response.json())
    .then(function (data) {
      console.log("hello");
      console.log(data);
      let strMealThumb = data.meals[0].strMealThumb;
      let strMeal = data.meals[0].strMeal;
      console.log(strMealThumb, data.meals[0].strMeal);
      console.log("salut");
      let ficherecette = new RecetteCard(strMealThumb, strMeal);

      //ajout Jasmine
      console.log(ficherecette.content);
      list.appendChild(ficherecette.content);
    });
}

console.log("recettes aléatoires");

const random = `https://www.themealdb.com/api/json/v1/1/random.php`;

for (let compteur = 0; compteur <= 5; compteur++) {
  fetch(random)
    .then((response) => response.json())
    .then(function (data) {
      //  let Recette = new RecetteCard();
      console.log(data);

      const { strMealThumb, strMeal } = data.meals[0];

      let ficherecette = new RecetteCard(strMealThumb, strMeal);
      console.log(ficherecette.content);
      list.appendChild(ficherecette.content);
    });
}
